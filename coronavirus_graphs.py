"""This script enables the user to plot graphs, charts and maps based on the coronavirus data retrieved in crawler and analyzed in coronavirus_statistics."""
from coronavirus_statistics import *
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy

def regions_piechart(region, show = False):
    """This functions takes a region and draws a piechart representing a share of coronavirus deaths and a share of people who recovered from the virus."""
    data = region_data()
    data = {line[0]: line[1:] for line in data}

    deaths = int(data[region][1])
    recoveries = int(data[region][0]) - deaths

    plt.pie((deaths, recoveries), labels = ["Number of deaths from coronavirus", "Number of recoveries from coronavirus"])
    plt.title(f"Share of deaths and recoveries from coronavirus in {region}")
    plt.savefig(f"./graphs/piechart_{region}.png")

    if show:
        plt.show()

def countries_barchart(list_countries = None, show = False):
    """This function takes a list of countries and draws a barchart showing the number of cases per one million of citizens, and a barchart showing the number of deaths per one million of citizens. Default value is list of all countries."""
    
    data = country_data(list_countries)
    
    countries = [0] * len(list(data))
    cases = [0] * len(list(data))
    deaths = [0] * len(list(data))

    for i, line in enumerate(data):
        countries[i] = line[0]
        cases[i] = float(line[1])
        deaths[i] = float(line[2])

    plt.bar(countries, cases)
    plt.title("Number of cases of coronavirus by 1 million citizens by country")
    plt.savefig("./graphs/barchart_cases.png")

    if show:
        plt.show()

    plt.bar(countries, deaths)
    plt.title("Number of deaths from coronavirus by 1 million citizens by country")
    plt.savefig("./graphs/{barchart_deaths}.png")

    if show:
        plt.show()

def highest_mortality(k, n = 0, show = False):
    """This function takes two integers n and k. It draws a barchart for the k countries with the highest number of deaths per one million of citizens with ppulation size at least n. Default value for n is 0."""

    data = top_country_data(k, n)

    countries = [0] * len(data)
    deaths = [0] * len(data) 

    for i, line in enumerate(data):
        countries[i] = line[0]
        deaths[i] = float(line[1])

    plt.bar(countries, deaths)
    plt.title("Top {} countries with highest mortality rates, excluding countries with less than {} inhabitants".format(k, n))
    plt.savefig("./graphs/mortality.png")

    if show:
        plt.show()

def map(k, show = False):
    """This function takes an integer k and draws a map showing the capital cities of k countries with highest number of deaths per one million of citizens. The size of the circle depends on the mortality rate."""

    data = top_country_data(k)
    ax = plt.axes(projection = ccrs.PlateCarree())

    for line in data:
        ax.plot(float(line[2]), float(line[3]), "bo", markersize = float(line[2])/100000, transform = ccrs.Geodetic())

    plt.savefig("./graphs/map.png")

    if show:
        plt.show()

def main():
    """This function interacts with the user."""
    
    print("Do you want to study the coronavirus data? (Yes/No)")
    command = input().lower()

    while command == "yes":
        print("""Which graph do you want to create? Please enter a letter (ex: 'A'). 

        \n A - Piechart: share of coronavirus deaths and recoveries in a particular region.

        \n B - Barcharts: number of cases per one million citizens, and number of deaths per one million citizens. 
        \n C - Barchart: countries with highest mortality rates. 

        \n D - Map: capital cities of countries with highest mortality rates.""")
        
        graph = input().lower()

        if graph == "a":        
            print("Please enter a region (ex: 'North America').")
            region = input()

            print("Do you want to see the graph? (Yes/No)")
            answer = input().lower()
            if answer == "yes":
                regions_piechart(region, show = True)
            else: 
                regions_piechart(region)

        if graph == "b":
            print("Please enter a list of countries separated by commas (ex: 'United States,France,Germany'). Press enter for all countries.")
            list_countries = input()

            print("Do you want to see the graph? (Yes/No)")
            answer = input().lower()
            if answer == "yes":
                countries_barchart(list_countries, show = True)
            else: 
                countries_barchart(list_countries)

        if graph == "c":
            print("Please enter an integer k to get the k countries with the highest mortality rates. Optionally, enter an integer n to exclude all countries with population smaller than n. Separate the two integers by a comma (ex: '5, 200000').")
            k_n = input()
            if "," in k_n:
                k, n = k_n.split(",")
                k, n = int(k), int(n)
            else:
                k = int(k_n)
                n = 0
                        
            print("Do you want to see the graph? (Yes/No)")
            answer = input().lower()
            if answer == "yes":
                highest_mortality(k, n, show = True)
            else: 
                highest_mortality(k, n)

        if graph == "d":
            print("Please enter an integer k to get the k countries with the highest mortality rates.")
            k = int(input())
 
            print("Do you want to see the graph? (Yes/No)")
            answer = input().lower()
            if answer == "yes":
                map(k, show = True)
            else: 
                map(k)

        print("Do you want to continue? (Yes/No)")
        command = input()
        show = False
        
if __name__ == "__main__": 
    main()

