"""This module calculates statistics based on coronavirus data retrieved from module crawler and placed into the file coronavirus_data.csv."""
import numpy as np

def read_data():
    """This function reads the coronavirus_data.csv file and loads it into a numpy arra."""
    data = np.loadtxt("coronavirus_data.csv", delimiter= ",", dtype=object)

    return data

def region_data():
    """This function retrieves information about world regions (number of cases, number of deaths, population)."""
    
    data = read_data()[1:]
    dict_region_data = {}

    for country in data:
        cases, deaths, region, population = int(country[1]), int(country[2]), country[3], int(country[4])
        if region not in dict_region_data:
            dict_region_data[region] = np.array([cases, deaths, population])

        else:
            dict_region_data[region] += np.array([cases, deaths, population])
    
    for region, data in dict_region_data.items():
        dict_region_data[region] = [region] + list(data)
        dict_region_data[region] = np.array(dict_region_data[region])

    return np.array([dict_region_data[region] for region in dict_region_data.keys()])

def country_data(list_countries = None):
    """This function returns information about a given list of country (number of cases and number of deaths normalized by the size of the population, poppulation)."""

    data = read_data()
    dict_country_data = {line[0]:line[1:] for line in data}

    if not list_countries: 
        list_countries = list(dict_country_data.keys())[1:]

    else:
        list_countries.split(",")

    data_countries = []

    for country in list_countries:
        cases = int(dict_country_data[country][0])
        deaths = int(dict_country_data[country][1])
        population = int(dict_country_data[country][3])

        data_countries.append(np.array([country, cases/population*1000000, deaths/population*1000000, population]))

    return np.array([data for data in data_countries])

def top_country_data(k, n = 0):
    """This function returns information about the k countries tht have highest number of deaths normalized by the size of the population, excluding country with population smaller than n."""

    data_countries = read_data()
    top_countries = []

    for i, country in enumerate(data_countries[1:]):
        if int(country[4]) >= n:
            country_info = np.array([country[0], int(country[2])/int(country[4])*1000000, country[5], country[6]])
            top_countries.append(country_info)

    data = sorted(top_countries, key = lambda a:-float(a[1])) #sort by mortality rate decreasing

    return np.array([np.array(x) for x in data[:k]])
