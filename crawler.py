"""This module reads coronavirus data from the web and from a .csv file. It writes a file coronavirus_data.csv containing the retrieved data."""

import numpy as np
import requests
from bs4 import BeautifulSoup

def cases_deaths():
    """This function retrieves coronavirus data from the worldometers website."""
    page = requests.get("https://www.worldometers.info/coronavirus/countries-where-coronavirus-has-spread/")
    soup = BeautifulSoup(page.content, "html.parser")
    
    data_tr = soup.find_all("tr")
    data = [0] * len(data_tr)
    
    for i, elt in enumerate(data_tr):
        data[i] = elt.find_all("td")
        data[i] = list(map(list(map(data[i], lambda x : str(x).split(">")[1]), lambda x : str(x).split("<")[0]))) #retrieve data between brackets

    data = data[1:] #remove first element (headers)

    for i, elt in enumerate(data):
        data[i] = list(map(data[i], lambda x : int(x.replace(",","")))) #removes commas from figures

        if elt[0] == "Japan (+Diamond Princess)":
            data[i][0] = "Japan"

        if elt[0] == "MS Zaandam":
            data.pop(i)

    data.sort(key = lambda a:a[0]) #sort by alphabetical order of country name

    return data

def population():
    """This function retrieves data from the worldometer website concerning the population size of countries in the world."""

    page = requests.get("https://www.worldometers.info/world-population/population-by-country/")
    soup = BeautifulSoup(page.content, "html.parser")

    data = soup.find_all("tr")

    data = data[1:]

    for i, elt in enumerate(data):
        data[i] = elt.find_all("td")
        data[i] = data[i][1:3]
        data[i][0] = data[i][0].find_all("a")

    for i, elt in enumerate(data):
        data[i] = list(map(list(map(data[i], lambda x : str(x).split(">")[1]), lambda x : str(x).split("<")[0])))
        data[i][1] = int(data[i][1].replace(",",""))

    data.sort(key = lambda a:a[0])

    return data

def capital_coordinates():
    """This function retrieves data from a .csv file containing information about capital cities."""
    
    data = np.loadtxt("worldcities.csv", delimiter= ",", dtype=object)
    capital_cities = list(map(filter(data, lambda x : x[8] == "primary"), lambda x : x[1:5])) #keep only capital cities

    for i, elt in enumerate(capital_cities):
        elt[0], elt[3] = elt[3], elt[0]

        for j in range(1,3):
            capital_cities[i][j] = float(capital_cities[i][j])

    capital_cities = list({line[3]:line for line in capital_cities}.values()) #remove additional capital city
    capital_cities.sort(key = lambda a:a[0])
   
    return capital_cities

def main():
    """This function writes a .csv file containing the following information about world countries: number of cases, number of deaths, region, population, latitude, longitude."""

    list_cases_deaths = cases_deaths()
    list_population = population()
    list_capital_coordinates = capital_coordinates()

    dict_cases_deaths  = {line[0]:line[1:] for line in list_cases_deaths}
    dict_population  = {line[0]:line[1:] for line in list_population}
    dict_capital_coordinates  = {line[0]:line[1:3] for line in list_capital_coordinates}

    data = []

    for line in list_capital_coordinates:
        country = line[0]

        if country not in dict_cases_deaths or country not in dict_population:
            continue

        data_line = [country] + dict_cases_deaths[country] + dict_population[country] + list(dict_capital_coordinates[country])
        data.append(data_line)

    file = open("coronavirus_data.csv", "w")
    file.write("Country,number_of_cases,number_of_deaths,region,population,latitude,longitude\n")

    for i, line in enumerate(data):
        data[i] = list(map(data[i], lambda x : str(x)))
        file.write(",".join(line) + "\n")

if __name__ == "__main__":
    main()
